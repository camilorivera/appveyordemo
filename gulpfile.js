var gulp = require('gulp');
var exec = require('child_process').exec;
const zip = require('gulp-zip');
const AWS = require('aws-sdk');
const gutil = require('gulp-util');
const fs = require('fs');

const environment = process.env.environment || 'local';
const appName = 'appveyordemo';
const version = '0.0.1';

AWS.config.update({
	accessKeyId: process.env.AWS_ACCESS_KEY_ID,
	secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
	region: 'us-east-1',
});

gulp.task('zip-app', function(cb){
	return gulp.src(['./package.json', './server.js', 'src/client/public/**/*.js','src/client/index.html'
		], {
			base: './'
		})
		.pipe(zip(`${appName}-${environment}.zip`))
		.pipe(gulp.dest('./dist'));
});

gulp.task('push-to-s3',['zip-app'], (done) => {
	const s3 = new AWS.S3();
	s3.createBucket({
		Bucket: appName,
	}, (err) => {
		if (err && err.code !== 'BucketAlreadyOwnedByYou') {
			throw new gutil.PluginError('push-to-s3', err);
		}
		s3.upload({
			Bucket: appName,
			Key: `${appName}-${environment}.zip`,
			Body: fs.createReadStream(`dist/${appName}-${environment}.zip`),
		}, (uploadErr) => {
			if (uploadErr) {
				throw new gutil.PluginError('push-to-s3', uploadErr);
			}
			done();
		});
	});
});

gulp.task('deploy', ['push-to-s3'], (done) => {
	const eb = new AWS.ElasticBeanstalk();
	eb.createApplicationVersion({
		ApplicationName: appName,
		VersionLabel: process.env.APPVEYOR_BUILD_VERSION || version,
		SourceBundle: {
			S3Bucket: appName,
			S3Key: `${appName}-${environment}.zip`,
		},
	}, (err) => {
		if (err) {
			throw new gutil.PluginError('update-elastic-beanstalk', err);
		}

		eb.updateEnvironment({
			ApplicationName: appName,
			EnvironmentName: `${appName}-${environment}`,
			VersionLabel: process.env.APPVEYOR_BUILD_VERSION || version,
		}, (updateEnvironmentErr) => {
			if (updateEnvironmentErr) {
				throw new gutil.PluginError('update-elastic-beanstalk', updateEnvironmentErr);
			}
			done();
		});
	});
});

gulp.task('build', function(cb) {
    exec('webpack -p', function(err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
    });
});

gulp.task('start', function (cb) {
  exec('node server.js', function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
})
